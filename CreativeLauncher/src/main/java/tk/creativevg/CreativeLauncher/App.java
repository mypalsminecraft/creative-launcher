package tk.creativevg.CreativeLauncher;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.Box;

public class App extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private LoginManager LoginManager = new LoginManager("http://login.creativevg.tk/");
	private String LoginToken;
	private Game Newverse = new Game("/path/to/game/", "java -jar");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App frame = new App();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public App() {
		setTitle("Creative Studios Launcher");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Games", null, panel, "Login and start games");
		
		Box usernameBox = Box.createVerticalBox();
		panel.add(usernameBox);
		
		JLabel lblUsername = new JLabel("Username");
		usernameBox.add(lblUsername);
		
		usernameField = new JTextField();
		usernameBox.add(usernameField);
		usernameField.setColumns(10);
		
		Box passwordBox = Box.createVerticalBox();
		panel.add(passwordBox);
		
		JLabel lblPassword = new JLabel("Password");
		passwordBox.add(lblPassword);
		panel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{usernameField, passwordField}));
		
		passwordField = new JPasswordField();
		passwordBox.add(passwordField);
		passwordField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		panel.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Select a Game", "Newverse"}));
		
		Box verticalBox = Box.createVerticalBox();
		panel.add(verticalBox);
		
		JButton btnLoginAndStart = new JButton("Start Game and Login");
		verticalBox.add(btnLoginAndStart);
		
        btnLoginAndStart.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e) {
		
		String username = usernameField.getText();
		char[] passchar = passwordField.getPassword();
		LoginManager.SetLogin(username, passchar);
	    this.LoginToken = LoginManager.Login();
	    Newverse.Launch(username, this.LoginToken);
	    passchar = null;
		
	}

}

class LoginManager {
	
	private String LoginSite;
	private String Charset;
	private String LoginQuery;
	private String Username;
	private String Password;
	private URLConnection Connection;
	private InputStream ConnectionResponse;
	
	LoginManager(String LoginSite) {
		
		this.LoginSite = LoginSite;
		this.Charset = "UTF-8";
		
	}
	
	LoginManager(String LoginSite, String Charset) {
		
		this.LoginSite = LoginSite;
		this.Charset = Charset;
		
	}
	
	public void SetLogin(String Username, char[] Password) {
		
		this.Username = Username;
		this.Password = Password.toString();
		Password = null;
		
	}
	
	@SuppressWarnings("deprecation")
	public String Login() {
		
		this.LoginQuery = String.format("username=%1&password=%2", URLEncoder.encode(this.Username), URLEncoder.encode(this.Password));
		try {
			this.Connection = new URL(this.LoginSite + "?" + this.LoginQuery).openConnection();
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			System.out.println("Reached exception MalformedURLException in the connection to the CS servers. Report at the Newverse Forums.");
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Reached exception IOException in the connection to the CS servers. Report at the Newverse Forums.");
			e1.printStackTrace();
		}
		this.Connection.setRequestProperty("Accept-Charset", this.Charset);
		this.Connection.setRequestProperty("User-Agent","JavaCreativeLauncher/0.0.1");
		try {
			this.ConnectionResponse = Connection.getInputStream();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Reached exception IOException in recieving information from the CS servers. Report at the Newverse forums.");
			e1.printStackTrace();
		}
		
		return ConnectionResponse.toString();
		
	}
	
}
