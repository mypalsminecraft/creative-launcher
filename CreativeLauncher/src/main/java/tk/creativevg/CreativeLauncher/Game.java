package tk.creativevg.CreativeLauncher;

import java.io.IOException;
import java.lang.Runtime;

public class Game {
	
	public String path;
	private String iprep;
	private Runtime Runtime;
	
	Game(String path, String interprepter) {
		
		this.path = path;
		this.iprep = interprepter;
		
	}
	
	public int Launch(String username, String logintoken) {
		
		try {
			
			return Runtime.exec(this.iprep + " " + this.path + " " + username + " " + logintoken).exitValue();
			
		} catch (IOException e) {
			
			return 1024;
			
		}
		
	}

}
